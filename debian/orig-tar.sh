#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>

TAR=$3

# clean up the upstream tarball
tar -x -z -f $TAR
tar -c -z -f $TAR --exclude '*.jar' --exclude '*/javadocs/*' trove-*/
rm -rf trove-*/

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi

